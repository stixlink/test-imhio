### Install
Before use fill configuration files ./config.toml and test configuration config_test.toml.
Example configure in files  config.toml.dist and config_test.toml.dist.

### Run server
If you want run tcp server, then run execute file with flag "--typeServer" value "tcp"
If you want http server, then run with flag "--typeServer" value "http"
Default value "tcp"
Test request for HTPP
```
curl --data '{"Type": "Develop.mr_robot","Data": "Database.processing"}' tcp://localhost:8081
```
Test request for tcp
```
netcat localhost 8081
#after connect insert data in console and press "enter"
{"Type": "Develop.mr_robot","Data": "Database.processing"}
```

### Run migrations in console
Build goose migrations binnary

```
cd ./cmd/migrations
go build -o  goose
```

Manage migration
(From dir projectDir/cmd/migrations)
```

Up migrations
./goose postgres "user=postgres dbname=imhio password=postgres sslmode=disable" up -dir=.

Up-to migrations
./goose postgres "user=postgres dbname=imhio password=postgres sslmode=disable" up-to VERSION -dir=.

Down migrations
./goose postgres "user=postgres dbname=imhio password=postgres sslmode=disable" down

Down-to migrations
./goose postgres "user=postgres dbname=imhio password=postgres sslmode=disable" down-to VERSION -dir=.

Redo migrations
./goose postgres "user=postgres dbname=imhio password=postgres sslmode=disable" redo

```
many info about migrations command see https://github.com/pressly/goose