package main

import (
	"testing"
	"github.com/davecgh/go-spew/spew"
	"fmt"
)


func TestGetConfigArgsPgsql(t *testing.T) {

	type TestCase struct {
		checkString string
		inputData   Database
		err         error
	}

	testCases := []TestCase{
		{
			checkString: "user=user password=passw dbname=imhio host=localhost port=5432 sslmode=disable",
			inputData: Database{
				Host:     "localhost",
				Port:     "5432",
				DbName:   "imhio",
				SslMode:  "disable",
				Password: "passw",
				User:     "user",
			},
			err: nil,
		},
		{
			checkString: "",
			inputData: Database{
				Host:     "localhost",
				Port:     "5432",
				DbName:   "imhio",
				SslMode:  "disable",
				Password: "passw",
				User:     "",
			},

			err: fmt.Errorf("All fields must be set (%s)", "user"),
		},
	}
	for _, tc := range testCases {
		res, err := GetConfigArgsPgSql(&tc.inputData)
		if err != tc.err && res != tc.checkString {
			t.Errorf("func: GetConfigArgsPgSql() returned an incorrect result!\n Result: \"%s\"; Check: \"%s\";\n err:%s; checkErr: %s ", spew.Sdump(res), spew.Sdump(tc.checkString), spew.Sdump(err), spew.Sdump(tc.err))
		}
	}
}

func TestGetConfigStringHttpServer(t *testing.T) {
	type TestCase struct {
		checkString string
		inputData   HttpServer
		err         error
	}

	testCases := []TestCase{
		{
			checkString: "localhost:8080",
			inputData: HttpServer{
				Host: "localhost",
				Port: "8080",
			},
			err: nil,
		},
		{
			checkString: "",
			inputData: HttpServer{
				Host: "localhost",
				Port: "",
			},
			err: fmt.Errorf("All fields must be set (%s)", "port"),
		},
	}
	for _, tc := range testCases {
		res, err := GetConfigStringHttpServer(&tc.inputData)
		if err != tc.err && res != tc.checkString {
			t.Errorf("func: GetConfigStringHttpServer() returned an incorrect result!\n Result: \"%s\"; Check: \"%s\";\n err:%s; checkErr: %s ", spew.Sdump(res), spew.Sdump(tc.checkString), spew.Sdump(err), spew.Sdump(tc.err))
		}
	}
}
