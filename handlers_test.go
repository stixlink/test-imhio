package main

import (
	"testing"
	"github.com/valyala/fasthttp"
	"os"
	"github.com/davecgh/go-spew/spew"
	"github.com/naoina/toml"
	"bitbucket.org/stixlink/test-imhio/cmd/message"
	"fmt"
	"encoding/json"
	"net"
	"sync"
)

type Processing struct {
	Host     string `json:"host"`
	Port     int    `json:"port"`
	Database string `json:"database"`
	User     string `json:"user"`
	Password string `json:"password"`
	Schema   string `json:"schema"`
}

type TestFillStruct struct {
	Processing Processing `json:"processing"`
}

func TestFindConfigHandler_HttpHandler(t *testing.T) {
	type TestCase struct {
		inputData     []byte
		response      string
		requestMethod string
		status        int
	}

	testCases := []TestCase{
		{
			inputData:     []byte(`{"Type": "Develop.mr_robot","Data": "Database"}`),
			response:      `{"processing":{"host":"localhost","port":5432,"database":"devdb","user":"mr_robot","password":"secret","schema":"public"}}`,
			requestMethod: "POST",
			status:        fasthttp.StatusOK,
		},
		{
			inputData:     []byte(`{"Type": "Develop.mr_robot","Data": "Database.processing"}`),
			response:      `{"database":"devdb","host":"localhost","password":"secret","port":5432,"schema":"public","user":"mr_robot"}`,
			requestMethod: "POST",
			status:        fasthttp.StatusOK,
		},
		{
			inputData:     []byte(`{"Type": "Develop.mr_robot","Data": "Database.processing.port"}`),
			response:      `5432`,
			requestMethod: "POST",
			status:        fasthttp.StatusOK,
		},
		{
			inputData:     []byte(`{"Type": "NotExist.mr_robot","Data": "Database.processing"}`),
			response:      message.ResponseErrorRequestBody,
			requestMethod: "POST",
			status:        fasthttp.StatusBadRequest,
		},
		{
			inputData:     []byte(`{"Type": "Develop.notExist","Data": "Database.processing"}`),
			response:      fmt.Sprintf(message.ResponseNotFound, "Develop.notExist"),
			requestMethod: "POST",
			status:        fasthttp.StatusNoContent,
		},
		{
			inputData:     []byte(`{"Type": "Develop.mr_robot","Data": "Database.processing.lolo"}`),
			response:      message.ResponseErrorRequestBody,
			requestMethod: "POST",
			status:        fasthttp.StatusBadRequest,
		},
	}

	var configFilePath = "./config_test.toml"
	f, err := os.Open(configFilePath)
	if nil != err {
		panic(spew.Errorf("Error open config file: %s \n, %s ", err, spew.Sdump(configFilePath)))
	}
	var config = &Config{}
	defer f.Close()
	if err := toml.NewDecoder(f).Decode(config); nil != err {
		panic(spew.Errorf("Error open config file: %s \n, %s ", err, spew.Sdump(config)))
	}
	db := initDb(&config.Database)
	defer closeDb(db)
	migrate(db)
	for i, tc := range testCases {
		req := &fasthttp.Request{}
		req.SetRequestURI("http://localhost:8081/config")
		req.Header.SetMethod("POST")
		req.SetBody(tc.inputData)

		ctx := &fasthttp.RequestCtx{}
		ctx.SetContentType("application/json")
		ctx.Init(req, nil, nil)
		handl := FindConfigHandler{Db: db}

		handl.HttpHandler(ctx)

		resp := ctx.Response.Body()
		if tc.status != ctx.Response.StatusCode() {
			t.Errorf("func: FindConfigHandler_Handler() returned an incorrect status code!\n Result: \"%v\"; Check: \"%v\";", ctx.Response.StatusCode(), tc.status)
			continue
		}
		if string(resp) != tc.response {
			if i == 0 {
				fst1 := TestFillStruct{}
				fst2 := TestFillStruct{}
				err := json.Unmarshal(resp, &fst1)
				if nil != err {
					t.Errorf("Error unmarshal body:\n %s \n ", spew.Sdump(fst1))
					continue
				}
				err = json.Unmarshal([]byte(tc.response), &fst2)
				if nil != err {
					t.Errorf("Error unmarshal body:\n %s \n ", spew.Sdump(fst2))
					continue
				}
				if fst1 != fst2 {
					t.Errorf("Result: \"%s\"; Check: \"%s\";", spew.Sdump(resp), spew.Sdump([]byte(tc.response)))
					continue
				}
				continue
			}
			t.Errorf("func: FindConfigHandler_Handler() returned an incorrect result!\n Result: \"%s\"; Check: \"%s\";", spew.Sdump(resp), spew.Sdump(tc.response))
			continue
		}
	}
}

func TestFindConfigHandler_TcpHandler(t *testing.T) {

	type TestCase struct {
		inputData string
		response  string
	}
	tcs := []TestCase{
		{
			inputData: `{"Type": "Develop.mr_robot","Data": "Database"}`,
			response:  `{"processing":{"host":"localhost","port":5432,"database":"devdb","user":"mr_robot","password":"secret","schema":"public"}}`,
		},
		{
			inputData: `NotValidJson: "Develop.mr_robot","Data": "Database.processing"}`,
			response:  message.ResponseErrorRequestBody,
		},
		{
			inputData: `{"Type": "NotValidData.mr_robot","Data": "Database"}`,
			response:  message.ResponseErrorRequestBody,
		},
		{
			inputData: `{"Type": "Develop.not_found","Data": "Database"}`,
			response:  fmt.Sprintf(message.ResponseNotFound, "Develop.not_found"),
		},
		{
			inputData: `{"Type": "Develop.mr_robot","Data": "Database.processing.lolo"}`,
			response:  message.ResponseErrorRequestBody,
		},
	}
	var configFilePath = "./config_test.toml"
	f, err := os.Open(configFilePath)
	if nil != err {
		panic(spew.Errorf("Error open config file: %s \n, %s ", err, spew.Sdump(configFilePath)))
	}
	var config = &Config{}
	defer f.Close()
	if err := toml.NewDecoder(f).Decode(config); nil != err {
		panic(spew.Errorf("Error open config file: %s \n, %s ", err, spew.Sdump(config)))
	}
	db := initDb(&config.Database)
	defer closeDb(db)
	migrate(db)
	laddr := &net.TCPAddr{IP: net.IPv4(127, 0, 0, 1), Port: 41459}
	daddr := &net.TCPAddr{IP: net.IPv4(127, 0, 0, 1), Port: 41459}

	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func(t *testing.T, wg *sync.WaitGroup) {
		t.Log("Run dial ")
		defer wg.Done()
		conn, err := net.DialTCP("tcp4", nil, daddr)
		if err != nil {
			t.Fatal(err)
		}

		defer conn.Close()
		for i, tc := range tcs {
			fmt.Fprintln(conn, tc.inputData)
			buff := make([]byte, 512)
			n, err := conn.Read(buff)
			if err != nil {
				return
			}
			resp := buff[:n]
			if 0 == i {
				fst1 := TestFillStruct{}
				fst2 := TestFillStruct{}
				err = json.Unmarshal(resp, &fst1)
				if nil != err {
					t.Errorf("Error unmarshal body:\n %s \n ", spew.Sdump(fst1))
					continue
				}
				err = json.Unmarshal([]byte(tc.response), &fst2)
				if nil != err {
					t.Errorf("Error unmarshal body:\n %s \n ", spew.Sdump(fst2))
					continue
				}
				if fst1 != fst2 {
					t.Errorf("Result: \"%s\"; Check: \"%s\";", spew.Sdump(buff), spew.Sdump([]byte(tc.response)))
					continue
				}
			} else {
				if string(resp) != tc.response {
					t.Errorf("func: FindConfigHandler_TcpHandler() returned an incorrect result!\n Result: \"%s\"; Check: \"%s\";", spew.Sdump(resp), spew.Sdump(tc.response))
					continue
				}
			}
		}
		return

	}(t, wg)

	wg.Add(1)
	go func(t *testing.T, wg *sync.WaitGroup) {

		t.Log("Run listener")

		defer wg.Done()
		listener, err := net.ListenTCP("tcp4", laddr)
		if err != nil {
			t.Fatal(err)
		}
		connect, err := listener.AcceptTCP()
		if err != nil {
			t.Fatal(err)
		}
		defer connect.Close()
		h := FindConfigHandler{Db: db}
		h.TcpHandler(connect)
	}(t, wg)
	wg.Wait()
}
