package main

import (
	"github.com/davecgh/go-spew/spew"
	"github.com/jinzhu/gorm"
	"github.com/pressly/goose"
	"log"
	"strings"
	"bitbucket.org/stixlink/test-imhio/cmd/constants"
)

var allowValueEnvironment = []string{constants.EnumEnvironmentProduction, constants.EnumEnvironmentDevelop, constants.EnumEnvironmentTest}

type QueryConfigure struct {
	Type   string   `json:"Type,required" json.len:"[1,120]"`
	Data   string   `json:"Data,required" json.len:"[1,120]"`
	Env    string   `json:"-"`
	Name   string   `json:"-"`
	Blocks []string `json:"-"`
}

func (qc *QueryConfigure) prepareData() (err error) {
	err = qc.ValidateInputData()
	if nil != err {
		log.Printf("Not valid data QueryConfigure::prepareData()\n %s", qc)
		return
	}
	params := strings.Split(qc.Type, ".")

	qc.Env = params[0]
	qc.Name = params[1]
	qc.Blocks = strings.Split(qc.Data, ".")
	return
}

//Validate input data receive from request
func (qc *QueryConfigure) ValidateInputData() (err error) {
	params := strings.Split(qc.Data, ".")
	if len(params) < 1 {
		err = spew.Errorf("Error input value field \"Data\": %s \n", qc.Data)
		return
	}
	params = strings.Split(qc.Type, ".")
	if len(params) != 2 {
		err = spew.Errorf("Error input value field \"Type\": %s \n", qc.Type)
		return
	}
	if !CheckEnvironmentOnAllowed(params[0]) {
		err = spew.Errorf("Not allowed value \"Type\": %s \n", qc.Type)
		return
	}
	return
}

//Check value environment on allowed
func CheckEnvironmentOnAllowed(e string) (allowValue bool) {
	for _, v := range allowValueEnvironment {
		if v == e {
			allowValue = true
			return
		}
	}
	return
}

//Return struct gorm model for find
func (qc *QueryConfigure) GetConditionStruct() (bc ConfigurationBlock, err error) {
	err = qc.prepareData()
	if nil != err {
		err = spew.Errorf("Error QueryConfigure::GetConditionStruct() -> ::prepareData() : %s \n %s \n", qc, err)
		return
	}
	bc = ConfigurationBlock{
		Name: qc.Name,
		Env:  qc.Env,
	}
	return
}

func initDb(cfg *Database) (db *gorm.DB) {
	argsStr, err := GetConfigArgsPgSql(cfg)
	if nil != err {
		panic(spew.Errorf("Failed config: %s \n, %s ", err, spew.Sdump(cfg)))
	}
	db, err = gorm.Open("postgres", argsStr)
	if nil != err {
		panic(spew.Errorf("Failed to connect database: %s \n, %s ", err, spew.Sdump(argsStr)))
	}
	return
}

func closeDb(db *gorm.DB) {
	db.Close()
}

func migrate(db *gorm.DB) {
	goose.SetDialect("postgres")
	if err := goose.Run("up", db.DB(), "."); nil != err {
		log.Fatalf("goose run: %v", err)
	}
}
