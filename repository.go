package main

import (
	"github.com/jinzhu/gorm"
	"github.com/davecgh/go-spew/spew"
	"encoding/json"
)

func findConfig(db *gorm.DB, cb ConfigurationBlock) (ConfigurationBlock, bool) {
	if db.Where("env = ? AND name=?", cb.Env, cb.Name).Find(&cb).RecordNotFound() {
		return cb, false
	}

	return cb, true
}

type ConfigurationBlock struct {
	gorm.Model
	Env        string `gorm:"type:envenum;not null"`
	Name       string `gorm:"type:varchar(150);not null;unique(name, env)"`
	Parameters string `gorm:"type:json;not null;default:'{}'"`
}

func (cb *ConfigurationBlock) GetFilterParameters(blocks []string) (res interface{}, err error) {
	err = json.Unmarshal([]byte(cb.Parameters), &res)
	if nil != err {
		err = spew.Errorf("Not found config sub block!")
		return
	}
	for _, k := range blocks {
		a, b := res.(map[string]interface{})
		if !b {
			err = spew.Errorf("Not found key in parameters ConfigurationBlock::GetFilterParameters()")
			return
		}
		if k == "" {
			res = a
			return
		}
		item, ok := a[k]
		if !ok {
			err = spew.Errorf("Not found key in parameters")
			res = nil
			return
		}
		res = item
	}
	return
}
