package main

import (
	"testing"
	"net/http"
	"fmt"
	"github.com/davecgh/go-spew/spew"
	"time"
	"github.com/jinzhu/gorm"
	"net"
	"sync"
)

func TestInitHttpServer(t *testing.T) {
	cfgServer := HttpServer{
		Host: "localhost",
		Port: "8388",
	}
	go initHttpServer(&cfgServer, &gorm.DB{})
	time.Sleep(5 * time.Second)
	r := http.Response{}
	_, err := http.Post(fmt.Sprintf("http://%s:%s/%s", cfgServer.Host, cfgServer.Port, "config"), "application/json", r.Body)
	if nil != err {
		t.Errorf("func: initHttpServer() returned error!\n Config: %s \n", spew.Sdump(cfgServer))
	}

}


func TestInitHttpServerOnErrorEmptyConfig(t *testing.T) {
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func(t *testing.T, wg *sync.WaitGroup) {
		defer wg.Done()
		defer func(t *testing.T) {
			if r := recover(); r != nil {
				return
			} else {
				t.Fatal("Error init server not call panic!")
			}

		}(t)
		cfgServer := HttpServer{}
		initHttpServer(&cfgServer, &gorm.DB{})
	}(t, wg)

	var afterFunc = func() {
		wg.Done()
		t.Fatal("Sever run with fail config!")
	}
	timer := time.AfterFunc(5*time.Second, afterFunc)
	wg.Wait()
	timer.Stop()
}

func TestHttpServer_GetConnectString(t *testing.T) {
	ts := HttpServer{}
	_, err := ts.GetConnectString()
	if nil == err {
		t.Fail()
	}
}

func TestInitTcpServer(t *testing.T) {
	cfgServer := TcpServer{
		Host: "localhost",
		Port: "8085",
	}
	go initTcpServer(&cfgServer, &gorm.DB{})
	time.Sleep(1 * time.Second)
	constr, err := cfgServer.GetConnectString()
	if err != nil {
		t.Fatal(err)
	}
	conn, err := net.Dial("tcp", constr)
	if nil != err {
		t.Fatal(err)
	}
	defer conn.Close()
}

func TestInitTcpServerOnError(t *testing.T) {
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func(t *testing.T, wg *sync.WaitGroup) {
		defer wg.Done()
		defer func(t *testing.T) {
			if r := recover(); r != nil {
				return
			} else {
				t.Fatal("Error init server not call panic!")
			}

		}(t)
		cfgServer := TcpServer{
			Host: "ERROR",
			Port: "8085",
		}
		initTcpServer(&cfgServer, &gorm.DB{})
	}(t, wg)

	var afterFunc = func() {
		wg.Done()
		t.Fatal("Sever run with fail config!")
	}
	timer := time.AfterFunc(5*time.Second, afterFunc)
	wg.Wait()
	timer.Stop()
}

func TestInitTcpServerOnErrorEmptyConfig(t *testing.T) {
	wg := &sync.WaitGroup{}
	wg.Add(1)
	go func(t *testing.T, wg *sync.WaitGroup) {
		defer wg.Done()
		defer func(t *testing.T) {
			if r := recover(); r != nil {
				return
			} else {
				t.Fatal("Error init server not call panic!")
			}

		}(t)
		cfgServer := TcpServer{}
		initTcpServer(&cfgServer, &gorm.DB{})
	}(t, wg)

	var afterFunc = func() {
		wg.Done()
		t.Fatal("Sever run with fail config!")
	}
	timer := time.AfterFunc(5*time.Second, afterFunc)
	wg.Wait()
	timer.Stop()
}

func TestTcpServer_GetConnectString(t *testing.T) {
	ts := TcpServer{}
	_, err := ts.GetConnectString()
	if nil == err {
		t.Fail()
	}
}
