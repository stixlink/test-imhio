package main

import (
	"github.com/davecgh/go-spew/spew"
	"github.com/buaazp/fasthttprouter"
	"log"
	"github.com/valyala/fasthttp"
	"github.com/jinzhu/gorm"
	"github.com/pkg/errors"
	"fmt"
	"net"
)

func initHttpServer(cfg *HttpServer, db *gorm.DB) {
	connStr, err := GetConfigStringHttpServer(cfg)
	if nil != err {
		panic(spew.Errorf("Failed config: %s \n, %s ", err, spew.Sdump(cfg)))
	}

	router := fasthttprouter.New()
	findConfigHandler := FindConfigHandler{
		Db: db,
	}
	router.POST("/", findConfigHandler.HttpHandler)

	log.Fatal(fasthttp.ListenAndServe(connStr, router.Handler))
}

func initTcpServer(cfg *TcpServer, db *gorm.DB) {
	conStr, err := cfg.GetConnectString()
	if err != nil {
		panic(err)
	}
	listner, err := net.Listen("tcp", conStr)
	if err != nil {
		panic(err)
	}
	for {
		conn, err := listner.Accept()
		if err != nil {
			panic(err)
		}
		handl := FindConfigHandler{Db: db}
		go handl.TcpHandler(conn)
	}
}

type ConnectSever interface {
	GetConnectString() (result string, err error)
}

type HttpServer struct {
	Host string `toml:"host"`
	Port string `toml:"port"`
}

func (cfg *HttpServer) GetConnectString() (result string, err error) {
	if cfg.Host == "" || cfg.Port == "" {
		err = errors.Errorf(
			"All fields must be set (%s)",
			spew.Sdump(cfg))
		return
	}
	result = fmt.Sprintf("%s:%s", cfg.Host, cfg.Port)
	return
}

type TcpServer struct {
	Host string `toml:"host"`
	Port string `toml:"port"`
}

func (cfg *TcpServer) GetConnectString() (result string, err error) {
	if cfg.Host == "" || cfg.Port == "" {
		err = errors.Errorf(
			"All fields must be set (%s)",
			spew.Sdump(cfg))
		return
	}
	result = fmt.Sprintf("%s:%s", cfg.Host, cfg.Port)
	return
}

func GetConfigStringHttpServer(cfg ConnectSever) (result string, err error) {
	result, err = cfg.GetConnectString()
	return
}

func GetConfigArgsPgSql(cfg *Database) (result string, err error) {
	if cfg.Host == "" || cfg.Port == "" || cfg.User == "" ||
		cfg.Password == "" || cfg.DbName == "" || cfg.SslMode == "" {
		err = errors.Errorf("All fields must be set (%s)", spew.Sdump(cfg))
		return
	}
	result = fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%s sslmode=%s",
		cfg.User, cfg.Password, cfg.DbName, cfg.Host, cfg.Port, cfg.SslMode)
	return
}
