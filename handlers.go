package main

import (
	"github.com/valyala/fasthttp"
	"github.com/jinzhu/gorm"
	"encoding/json"
	"github.com/davecgh/go-spew/spew"
	"github.com/pkg/errors"
	"log"
	"bitbucket.org/stixlink/test-imhio/cmd/message"
	"fmt"
	"net"
	"bufio"
)

type FindConfigHandler struct {
	Db *gorm.DB
}

func (fch *FindConfigHandler) TcpHandler(conn net.Conn) {

	scanner := bufio.NewScanner(conn)
	for scanner.Scan() {
		query := QueryConfigure{}
		data := []byte(scanner.Text())
		err := json.Unmarshal(data, &query)
		if nil != err {
			SetResponse(conn, []byte(message.ResponseErrorRequestBody))
			log.Print(errors.Errorf("Error unmarshal body:\n %s \n ", spew.Sdump(data)))
			continue
		}
		err = query.ValidateInputData()
		if nil != err {
			SetResponse(conn, []byte(message.ResponseErrorRequestBody))
			log.Print(errors.Errorf("Received data not valid:\n %s \n ", spew.Sdump(data)))
			continue
		}
		cb, err := query.GetConditionStruct()
		if nil != err {
			SetResponse(conn, []byte(message.ResponseErrorRequestBody))
			log.Print(errors.Errorf("Received data not valid:\n %s \n ", spew.Sdump(data)))
			continue
		}
		cfgBlock, ok := findConfig(fch.Db, cb)
		if !ok {
			SetResponse(conn, []byte(fmt.Sprintf(message.ResponseNotFound, query.Type)))
			log.Print(errors.Errorf("Error unmarshal body:\n %s \n ", spew.Sdump(data)))
			continue
		}
		response, err := cfgBlock.GetFilterParameters(query.Blocks)
		if nil != err {
			SetResponse(conn, []byte(message.ResponseErrorRequestBody))
			log.Print(errors.Errorf("Received data not valid:\n %s \n ", spew.Sdump(data)))
			continue
		}
		json, err := json.Marshal(response)
		if nil != err {
			SetResponse(conn, []byte(message.ResponseInternalError))
			log.Print(errors.Errorf("Error unmarshal body:\n %s \n", spew.Sdump(data)))
			continue
		}
		SetResponse(conn, json)
	}
	return
}

func (fch *FindConfigHandler) HttpHandler(ctx *fasthttp.RequestCtx) {
	query := QueryConfigure{}
	data := ctx.Request.Body()
	err := json.Unmarshal(data, &query)
	if nil != err {
		SetStatusBadRequest(ctx, []byte(message.ResponseErrorRequestBody))
		log.Print(errors.Errorf("Error unmarshal body:\n %s \n ", spew.Sdump(data)))
		return
	}
	err = query.ValidateInputData()
	if nil != err {
		SetStatusBadRequest(ctx, []byte(message.ResponseErrorRequestBody))
		log.Print(errors.Errorf("Received data not valid:\n %s \n ", spew.Sdump(data)))
		return
	}
	cb, err := query.GetConditionStruct()
	if nil != err {
		SetStatusBadRequest(ctx, []byte(message.ResponseErrorRequestBody))
		log.Print(errors.Errorf("Received data not valid:\n %s \n ", spew.Sdump(data)))
		return
	}
	cfgBlock, ok := findConfig(fch.Db, cb)
	if !ok {
		SetStatusNoContent(ctx, []byte(fmt.Sprintf(message.ResponseNotFound, query.Type)))
		log.Print(errors.Errorf("Error unmarshal body:\n %s \n ", spew.Sdump(data)))
		return
	}
	response, err := cfgBlock.GetFilterParameters(query.Blocks)
	if nil != err {
		SetStatusBadRequest(ctx, []byte(message.ResponseErrorRequestBody))
		log.Print(errors.Errorf("Received data not valid:\n %s \n ", spew.Sdump(data)))
		return
	}
	json, err := json.Marshal(response)
	if nil != err {
		SetStatusBadGateway(ctx, []byte(message.ResponseInternalError))
		log.Print(errors.Errorf("Error unmarshal body:\n %s \n", spew.Sdump(data)))
		return
	}

	SetStatusOk(ctx, json)
	return
}

func SetResponse(conn net.Conn, data []byte) {
	conn.Write(data)
}

func SetStatusOk(ctx *fasthttp.RequestCtx, body []byte) {
	ctx.SetStatusCode(fasthttp.StatusOK)
	ctx.SetBody(body)
}

func SetStatusNoContent(ctx *fasthttp.RequestCtx, body []byte) {
	ctx.SetStatusCode(fasthttp.StatusNoContent)
	ctx.SetBody(body)
}

func SetStatusBadRequest(ctx *fasthttp.RequestCtx, body []byte) {
	ctx.SetStatusCode(fasthttp.StatusBadRequest)
	ctx.SetBody(body)
}

func SetStatusBadGateway(ctx *fasthttp.RequestCtx, body []byte) {
	ctx.SetStatusCode(fasthttp.StatusBadGateway)
	ctx.SetBody(body)
}
