package main

import (
	"testing"
	"github.com/davecgh/go-spew/spew"
	"encoding/json"
)

func TestConfigurationBlock_GetFilterParameters(t *testing.T) {

	type TestCase struct {
		inputData string
		result    string
		err       error
	}

	testCases := []TestCase{
		{
			inputData: `{"Database":{"processing":{"host":"localhost","port":5432,"database":"devdb","user":"mr_robot","password":"secret","schema":"public"}}}`,
			result:    `{"database":"devdb","host":"localhost","password":"secret","port":5432,"schema":"public","user":"mr_robot"}`,
			err:       nil,
		},
		{
			inputData: `{"notOneKey":{"notTwoKey":{"host":"localhost","port":5432,"database":"devdb","user":"mr_robot","password":"secret","schema":"public"}}}`,
			result:    `{"database":"devdb","host":"localhost","password":"secret","port":5432,"schema":"public","user":"mr_robot"}`,
			err:       spew.Errorf("Not found key in parameters"),
		},
		{
			inputData: `{"Database":{"notTwoKey":{"host":"localhost","port":5432,"database":"devdb","user":"mr_robot","password":"secret","schema":"public"}}}`,
			result:    `{"database":"devdb","host":"localhost","password":"secret","port":5432,"schema":"public","user":"mr_robot"}`,
			err:       spew.Errorf("Not found key in parameters"),
		},
		{
			inputData: `{"Database":"","bla":{"notTwoKey":{"host":"localhost","port":5432,"database":"devdb","user":"mr_robot","password":"secret","schema":"public"}}}`,
			result:    `{"database":"devdb","host":"localhost","password":"secret","port":5432,"schema":"public","user":"mr_robot"}`,
			err:       spew.Errorf("Not found key in parameters ConfigurationBlock::GetFilterParameters()"),
		},
		{
			inputData: `{notValidJson"`,
			result:    ``,
			err:       spew.Errorf("Not found config sub block!"),
		},
	}
	for _, tc := range testCases {
		cb := ConfigurationBlock{Parameters: tc.inputData}
		res, err := cb.GetFilterParameters([]string{"Database", "processing"})
		if nil == err {
			if err != tc.err {
				t.Errorf("func: ConfigurationBlock_GetFilterParameters() returned an incorrect result!\n Result: \"%s\"; Check: \"%s\"; Error:\"%s\"", spew.Sdump(res), spew.Sdump(tc.result), spew.Sdump(err))
				continue
			}
		} else {
			if tc.err.Error() != err.Error() {
				t.Errorf("func: ConfigurationBlock_GetFilterParameters() returned an incorrect result!\n Result: \"%s\"; Check: \"%s\"; Error:\"%s\"", spew.Sdump(res), spew.Sdump(tc.result), spew.Sdump(err))
				continue
			}
		}
		if nil == err {
			byteRes, err := json.Marshal(res)
			if nil != err {
				t.Errorf("func: ConfigurationBlock_GetFilterParameters():failed marshal result!\n  Error:\"%s\"", spew.Sdump(err))
				continue
			}
			if tc.result != string(byteRes) {
				t.Errorf("func: ConfigurationBlock_GetFilterParameters() returned an incorrect result!\n Result: \"%s\"; Check: \"%s\";", spew.Sdump(string(byteRes)), spew.Sdump(tc.result))
			}
		}
	}
}
