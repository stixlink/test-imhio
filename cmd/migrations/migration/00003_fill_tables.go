package migration

import (
	"database/sql"
	"github.com/pressly/goose"
	"bitbucket.org/stixlink/test-imhio/cmd/constants"
	"fmt"
)

func init() {
	goose.AddMigration(Up00003, Down00003)
}

func Up00003(tx *sql.Tx) error {
	_, err := tx.Exec(
		fmt.Sprintf(`INSERT INTO "configuration_blocks"(
							    "env",
							    "name",
							    "parameters"
							) VALUES (
							    '%s',
							    'vpn',
							    '{"Rabbit":{"log":{"host": "10.0.5.42","port": "5671","virtualhost": "/","user": "guest","password": "guest"}}}'
							 ),(
							    '%s',
							    'mr_robot',
							    '{"Database":{"processing":{"host":"localhost","port":5432,"database":"devdb","user":"mr_robot","password":"secret","schema":"public"}}}'
							 );
	`, constants.EnumEnvironmentTest, constants.EnumEnvironmentDevelop),
	)
	if nil != err {
		return fmt.Errorf("Error up migration 00003_fill_tables: " + err.Error())
	}
	return nil
}

func Down00003(tx *sql.Tx) error {
	_, err := tx.Exec(`TRUNCATE "configuration_blocks";`)
	if nil != err {
		return fmt.Errorf("Error up migration 00003_fill_tables: " + err.Error())
	}
	return nil
}
