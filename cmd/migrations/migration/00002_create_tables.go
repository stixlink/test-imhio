package migration

import (
	"database/sql"
	"github.com/pressly/goose"
	"fmt"
)

func init() {
	goose.AddMigration(Up00002, Down00002)
}

func Up00002(tx *sql.Tx) error {
	_, err := tx.Exec(
		`
			CREATE SEQUENCE IF NOT EXISTS "configuration_blocks_id_seq" START 1;`)
	if nil != err {
		return fmt.Errorf("Error up migration 00002_create_tables:\n error create SEQUENCE - \"configuration_blocks_id_seq\":\n " + err.Error())
	}
	_, err = tx.Exec(
		`
			CREATE TABLE IF NOT EXISTS "configuration_blocks" ( 
				"id" Integer DEFAULT nextval('configuration_blocks_id_seq'::regclass) NOT NULL,
				"created_at" Timestamp With Time Zone,
				"updated_at" Timestamp With Time Zone,
				"deleted_at" Timestamp With Time Zone,
				"env" "envenum" NOT NULL,
				"name" Character Varying( 150 ) NOT NULL,
				"parameters" JSON DEFAULT '{}'::json NOT NULL,
				PRIMARY KEY ( "id" ),
				CONSTRAINT "unique_name_env" UNIQUE( "name", "env" ) );	
				`)
	if nil != err {
		return fmt.Errorf("Error up migration 00002_create_tables:\n error create table \"configuration_blocks\":\n " + err.Error())
	}

	return nil
}

func Down00002(tx *sql.Tx) error {
	_, err := tx.Exec(`DROP TABLE IF EXISTS "configuration_blocks" CASCADE;`)
	if nil != err {
		return fmt.Errorf("Error up migration 00002_create_tables:\n error drop table \"configuration_blocks\":\n" + err.Error())
	}
	// This code is executed when the migration is rolled back.
	return nil
}
