package migration

import (
	"database/sql"
	"github.com/pressly/goose"
	"fmt"
	"bitbucket.org/stixlink/test-imhio/cmd/constants"
)

func init() {
	goose.AddMigration(Up00001, Down00001)
}

func Up00001(tx *sql.Tx) error {
str:=fmt.Sprintf(
	"DO $$ BEGIN IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'envenum') THEN CREATE TYPE envenum AS ENUM ('%s', '%s', '%s'); END IF; END$$;",
	constants.EnumEnvironmentTest,
	constants.EnumEnvironmentDevelop,
	constants.EnumEnvironmentProduction)
fmt.Println(str)
	_, err := tx.Exec(
		str,
	)
	if nil != err {
		return fmt.Errorf("Error up migration 00001_create_type: " + err.Error())
	}
	return nil
}

func Down00001(tx *sql.Tx) error {
	_, err := tx.Exec("DO $$ BEGIN IF EXISTS (SELECT 1 FROM pg_type WHERE typname = 'envenum') THEN DROP TYPE envenum; END IF; END$$;")
	if nil != err {
		return fmt.Errorf("Error down migration 00001_create_type: " + err.Error())
	}
	return nil
}
