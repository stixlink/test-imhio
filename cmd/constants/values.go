package constants

//Values for database
//Vales for ENUM environment configuration table
const (
	EnumEnvironmentDevelop    = "Develop"
	EnumEnvironmentTest       = "Test"
	EnumEnvironmentProduction = "Prod"
)

//Type server
const (
	ServerTypeTcp  = "tcp"
	ServerTypeHttp = "http"
)
