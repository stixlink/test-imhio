package message

//Message for response
const (
	ResponseErrorRequestBody = "Failed request body!"
	ResponseNotFound         = "Config \"%s\" not found!"
	ResponseInternalError    = "Internal error!"
)
