package main

import (
	"flag"
	"os"
	"github.com/naoina/toml"
	"github.com/jinzhu/gorm"
	"github.com/davecgh/go-spew/spew"
	_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "bitbucket.org/stixlink/test-imhio/cmd/migrations/migration"
	"bitbucket.org/stixlink/test-imhio/cmd/constants"
)

var (
	db             *gorm.DB
	err            error
	config         *Config
	configFilePath = flag.String("configFilePath", "./config.toml", "Path to config file")
	typeServer     = flag.String("typeServer", constants.ServerTypeTcp, "Protocol listeners server")
)

type Config struct {
	Database   Database
	HttpServer HttpServer
	TcpServer  TcpServer
}

type Database struct {
	Host     string `toml:"host"`
	Port     string `toml:"port"`
	User     string `toml:"user"`
	Password string `toml:"password"`
	DbName   string `toml:"dbname"`
	SslMode  string `toml:"sslmode"`
	Scheme   string `toml:"scheme"`
}

func init() {
	flag.Parse()
	f, err := os.Open(*configFilePath)
	if nil != err {
		panic(spew.Errorf("Error open config file: %s \n, %s ", err, spew.Sdump(configFilePath)))
	}
	config = &Config{}
	defer f.Close()
	if err := toml.NewDecoder(f).Decode(config); nil != err {
		panic(spew.Errorf("Error open config file: %s \n, %s ", err, spew.Sdump(config)))
	}
}

func main() {
	db = initDb(&config.Database)
	defer closeDb(db)
	migrate(db)
	if *typeServer == constants.ServerTypeTcp {
		initTcpServer(&config.TcpServer, db)
	} else if *typeServer == constants.ServerTypeHttp {
		initHttpServer(&config.HttpServer, db)
	} else {
		panic(spew.Errorf("Not valid parameter type server: %s \n, %s ", typeServer, err))
	}
}
