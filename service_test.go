package main

import (
	"testing"
	"github.com/davecgh/go-spew/spew"
	"bitbucket.org/stixlink/test-imhio/cmd/constants"
	"fmt"
)

func TestQueryConfigure_GetCriteria(t *testing.T) {
	qc := QueryConfigure{Type: "Develop.mr_robots", Data: "Database.processing"}
	checkCc := ConfigurationBlock{
		Env:  constants.EnumEnvironmentDevelop,
		Name: "mr_robots",
	}
	cc, err := qc.GetConditionStruct()
	if nil != err || !checkEqualsField(cc, checkCc) {
		t.Errorf("func: QueryConfigure_GetCriteria() returned an incorrect result!\n Result: \"%s\"; Check: \"%s\";\n err:%s; ", spew.Sdump(cc), spew.Sdump(checkCc), spew.Sdump(err))
	}
}

func checkEqualsField(cb1 ConfigurationBlock, cb2 ConfigurationBlock) bool {
	if cb1.Name == cb2.Name && cb1.Env == cb2.Env {
		return true
	}
	return false
}

func TestQueryConfigure_ValidateInputData(t *testing.T) {
	type TestCase struct {
		inputData QueryConfigure
		err       error
	}

	testCases := []TestCase{
		{
			inputData: QueryConfigure{Type: fmt.Sprintf("%s.%s", constants.EnumEnvironmentTest, "test1"), Data: "Data.data1"},
			err:       nil,
		},
		{
			inputData: QueryConfigure{Type: fmt.Sprintf("%s.%s", constants.EnumEnvironmentTest, "test2"), Data: ""},
			err:       nil,
		},
		{
			inputData: QueryConfigure{Type: fmt.Sprintf("%s", constants.EnumEnvironmentTest), Data: "Data.data3"},
			err:       spew.Errorf("Error input value field \"Type\": %s \n", constants.EnumEnvironmentTest),
		},
		{
			inputData: QueryConfigure{Type: fmt.Sprintf("%s.%s", "NotAllowed", "test4"), Data: "Data.data3"},
			err:       spew.Errorf("Not allowed value \"Type\": %s \n", fmt.Sprintf("%s.%s", "NotAllowed", "test4")),
		},
	}
	for _, tc := range testCases {
		err := tc.inputData.ValidateInputData()
		if nil == err {
			if err != tc.err {
				t.Errorf("func: QueryConfigure_ValidateInputData() returned an incorrect result!\n Result: \"%s\";\n err:%s; checkErr: %s ", spew.Sdump(tc.inputData), spew.Sdump(err), spew.Sdump(tc.err))
			}
		} else {
			if err.Error() != tc.err.Error() {
				t.Errorf("func: QueryConfigure_ValidateInputData() returned an incorrect result!\n Result: \"%s\";\n err:%s; checkErr: %s ", spew.Sdump(tc.inputData), spew.Sdump(err), spew.Sdump(tc.err))
			}
		}
	}
}

func TestCheckEnvironmentOnAllowed(t *testing.T) {
	type TestCase struct {
		inputData string
		result    bool
	}

	testCases := []TestCase{
		{
			inputData: "fake",
			result:    false,
		},
		{
			inputData: constants.EnumEnvironmentTest,
			result:    true,
		},
	}
	for _, tc := range testCases {
		res := CheckEnvironmentOnAllowed(tc.inputData)
		if res != tc.result {
			t.Errorf("func: CheckEnvironmentOnAllowed() returned an incorrect result!\n Result: \"%s\"; Check: \"%s\";", spew.Sdump(res), spew.Sdump(tc.result))
		}
	}

}

func TestQueryConfigure_prepareData(t *testing.T) {
	qc := QueryConfigure{}

	err := qc.prepareData()
	if nil == err {
		t.Error("")
	}

	qc.Type = "Develop.mr_robot"
	qc.Data = "Rabbit.log"

	err = qc.prepareData()
	if nil != err {
		t.Fail()
	}
}
